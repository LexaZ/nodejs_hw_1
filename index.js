const express = require("express");
const morgan = require("morgan");
const fs = require("fs").promises;
const fsSync = require("fs");
const path = require("path");

const app = express();
app.use(express.json());
app.use(morgan("dev"));

if (!fsSync.existsSync('files')){
    fsSync.mkdirSync('files');
}

app.post("/api/files", async (request, response) => {
    const files = await fs.readdir("files");
    const requestedFile = request.body.filename;
    const supportedFormats = /.*(log|txt|json|yaml|xml|js)$/;

    if (!request.body.filename) {
        return response.status(400).json({ "message": "Please specify 'filename' parameter" });
    } else if (!request.body.content) {
        return response.status(400).json({ "message": "Please specify 'content' parameter" });
    } else if (!supportedFormats.test(request.body.filename)) {
        return response.status(400).json({ "message": "Please specify appropriate file format" });
    } else if (files.indexOf(requestedFile) !== -1) {
        return response.status(400).json({ "message": `A file named '${requestedFile}' already exists` });
    }

    fs.writeFile(`files/${request.body.filename}`, `${request.body.content}`, "utf-8", (err) => {
        if (err) throw err;
    });

    response.json({ "message": "File created successfully" })
})

app.get("/api/files", async (request, response) => {
    const files = await fs.readdir("files");
    response.json({ "message": "Success", "files": files })
})

app.get("/api/files/:filename", async (request, response) => {
    const files = await fs.readdir("files");
    const requestedFile = request.params.filename;

    if (files.indexOf(requestedFile) === -1) {
        return response.status(400).json({ "message": `No file with '${requestedFile}' filename found` });
    }

    const requestedFileContent = await fs.readFile(`files/${requestedFile}`, "utf-8");
    const requestedFileExtension = path.extname(`files/${requestedFile}`).slice(1);
    const stats = await fs.stat(`files/${request.params.filename}`, function (err, stats) {
        if (err) throw err;
    });

    response.json({
        "message": "Success",
        "filename": requestedFile,
        "content": requestedFileContent,
        "extension": requestedFileExtension,
        "uploadedDate": stats.mtime
    })
})

app.put("/api/files/:filename", async (request, response) => {
    const files = await fs.readdir("files");
    const requestedFile = request.params.filename;

    if (files.indexOf(requestedFile) === -1) {
        return response.status(400).json({ "message": `No file with '${requestedFile}' filename found` });
    } else if (!request.body.newContent) {
        return response.status(400).json({ "message": "Please specify 'newContent' parameter" });
    }

    await fs.writeFile(`files/${requestedFile}`, `${request.body.newContent}`, "utf-8", (err) => {
        if (err) throw err;
    });

    response.json({ "message": "File modified successfully" })
})

app.delete("/api/files/:filename", async (request, response) => {
    const files = await fs.readdir("files");
    const requestedFile = request.params.filename;

    if (files.indexOf(requestedFile) === -1) {
        return response.status(400).json({ "message": `No file with '${requestedFile}' filename found` });
    }

    await fs.unlink(`files/${requestedFile}`)

    response.json({ "message": "File deleted successfully" })
})

app.use(function (err, request, response, next) {
    console.error(err.stack);
    response.status(500).json({ "message": "Server error" });
});

app.listen(8080)
